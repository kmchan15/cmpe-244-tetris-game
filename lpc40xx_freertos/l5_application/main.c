#include "stdio.h"

#include "FreeRTOS.h"
#include "task.h"

#include "GPIO1.h"
#include "cli_handlers.h"
#include "clock.h"
#include "delay.h"
#include "event_groups.h"
#include "ff.h"
#include "gpio.h"
#include "led_display_driver.h"
#include "lpc40xx.h"
#include "sj2_cli.h"
#include "string.h"
#include "tetris.h"
#include "tetris_menu.h"
#include "uart.h"
#include <stdlib.h>

TaskHandle_t producer_handle;
TaskHandle_t consumer_handle;
TaskHandle_t game_logic_handle;
TaskHandle_t game_over_handle;
TaskHandle_t title_color_handle;
TaskHandle_t game_player_logic_handle;
TaskHandle_t game_timer_handle;
TaskHandle_t game_menu_title;
TaskHandle_t game_menu_handle;

piece_in_play current_piece;

static uint8_t saved_piece_id;
static uint8_t save_piece_state = 0;
bool save_was_pressed = false;
bool enable_piece_swap = false;
bool game_over_set = false;
static int difficulty_speed = 0;
static int last_score = 1337;

void title_color(void *p) {
  vTaskSuspend(NULL);
  while (1) {
    for (int a = 0; a < 5; a++) {
      for (int i = 31; i > -1; i--) {
        if (a < 1)
          game_title_change(i, 0);
        else {
          game_title_change(i, 1);
        }
        vTaskDelay(50);
      }
      for (int i = 0; i < 32; i++) {
        if (a < 1)
          game_title_change(i, 0);
        else {
          game_title_change(i, 1);
        }
        vTaskDelay(50);
      }

      vTaskDelay(2000);
    }
  }
}
void display(void *p) {
  while (1) {
    led_display_draw_frame();
    vTaskDelay(10);
  }
}

void game_logic(void *p) {

  vTaskSuspend(NULL);
  piece_in_play r1_piece;
  piece_in_play r2_piece;
  piece_in_play r3_piece;
  int score = 1234;
  uint8_t color = 0;
  time_t t;
  srand((unsigned)time(&t));
  char stop = 's';

  current_piece = init_play_piece(3);
  r1_piece = init_play_piece(3);
  r2_piece = init_play_piece(3);
  r3_piece = init_play_piece(3);
  game_board_next_piece(r1_piece.color, 0);
  game_board_next_piece(r2_piece.color, 1);
  game_board_next_piece(r3_piece.color, 2);

  game_board_score_draw(0);
  game_draw_game_board(0);

  while (1) {
    if (game_over_set) {
      // send stop to board 2 to stop music
      uart__polled_put(UART__3, stop);
      score = 0;
      vTaskResume(game_menu_handle);
      vTaskSuspend(NULL);
    }
    if (game_place_piece(&current_piece)) {
      ;
    } else {
      last_score = score;
      vTaskResume(game_over_handle);
    }
    if (save_was_pressed == true) {
      if (save_piece_state == 1) {
        // Clear the current piece in play
        game_draw_piece(&current_piece, 0, 0, 0, 1);
        // Swap the saved/hold piece and current piece in play.
        current_piece = init_play_piece(saved_piece_id);
        save_piece_state = save_piece_state + 1;
        save_was_pressed = false;

      } else if (save_piece_state == 0) {
        // Initial piece save state.
        game_draw_piece(&current_piece, 0, 0, 0, 1);
        game_board_next_piece(r2_piece.color, 0);
        game_board_next_piece(r3_piece.color, 1);
        current_piece = r1_piece;
        r1_piece = r2_piece;
        r2_piece = r3_piece;

        if (color == 7) {
          color = 1;
        } else {
          color++;
        }
        r3_piece = init_play_piece(color);
        game_board_next_piece(r3_piece.color, 2);
        save_piece_state = save_piece_state + 1;
        save_was_pressed = false;
      } else if (save_piece_state == 2) {
        // do nothing...until piece placement is complete
        // so that the player cannot abuse swapping.
        save_was_pressed = false;
      }
    }
    if (current_piece.complete == 1) {
      score += game_check_full_lines() * 100;
      game_board_score_draw(score);

      // swap is enabled once a piece-in-play which followed a save press is placed/complete.
      if (save_piece_state == 2) {
        save_piece_state = 1;
        enable_piece_swap = true;
      }

      game_board_next_piece(r2_piece.color, 0);
      game_board_next_piece(r3_piece.color, 1);
      current_piece = r1_piece;
      r1_piece = r2_piece;
      r2_piece = r3_piece;
      // r2_piece = init_play_piece(3);

      if (color == 7) {
        color = 1;
      } else {
        color++;
      }
      // r3_piece = init_play_piece((rand() % 7) + 1);
      r3_piece = init_play_piece(color);
      game_board_next_piece(r3_piece.color, 2);
    }
    vTaskDelay(difficulty_speed);
  }
}
void clear_board_animation(void *p) {
  vTaskSuspend(NULL);
  while (1) {
    vTaskSuspend(game_logic_handle);
    for (int i = 0; i < 24; i++) {
      board_clear_game_board(i);
      vTaskDelay(75);
    }
    game_board_score_draw(0);
    vTaskResume(game_logic_handle);
    game_over_set = true;
    vTaskSuspend(title_color_handle);
    vTaskSuspend(NULL);
  }
}

void timer(void *p) {
  vTaskSuspend(NULL);
  int count = 0;
  while (1) {
    if (game_over_set == true) {
      count = 0;
      vTaskSuspend(NULL);
    }
    game_board_timer(count);
    vTaskDelay(1000);
    count++;
  }
}
void menu_title(void *p) {

  while (1) {
    for (int i = 31; i > -1; i--) {
      tetris__menu_draw_name(i);
      vTaskDelay(50);
    }
  }
}

void menu_logic(void *p) {
  gpio0__set_as_input(6, 0);
  gpio0__set_as_input(29, 0);
  gpio0__set_as_input(15, 1);
  gpio0__set_as_input(19, 1);
  LPC_IOCON->P1_15 &= ~(3 << 3);
  LPC_IOCON->P1_15 |= (1 << 3);
  LPC_IOCON->P1_19 &= ~(3 << 3);
  LPC_IOCON->P1_19 |= (1 << 3);

  char play = 'p';
  int game_mode_option = 1;
  tetris__draw_menu(game_mode_option, 0);
  tetris__menu_score_draw(1337);
  difficulty_speed = 500;
  while (1) {

    if (gpio0__get_level(6, 0)) {
      if (game_mode_option == 0) {
        difficulty_speed = 1000;
      } else if (game_mode_option == 1) {
        difficulty_speed = 500;
      } else if (game_mode_option == 2) {
        difficulty_speed = 100;
      }
      tetris__draw_menu(game_mode_option, 0);
      game_mode_option++;
      game_mode_option %= 3;
      vTaskDelay(200);
    }
    if (gpio0__get_level(9, 0)) {
      vTaskResume(game_logic_handle);
      vTaskResume(title_color_handle);
      vTaskResume(game_timer_handle);
      vTaskResume(game_player_logic_handle);
      game_over_set = false;
      // Send play byte to board2 to start playing music
      uart__polled_put(UART__3, play);
      vTaskSuspend(game_menu_title);
      game_board_save_piece(0);
      game_draw_display();
      game_draw_game_board(0);
      game_board_timer(0);
      save_piece_state = 0;
      save_was_pressed = false;
      enable_piece_swap = false;
      vTaskSuspend(NULL);
      vTaskResume(game_menu_title);
      tetris__draw_menu(game_mode_option, 1);
      tetris__menu_score_draw(last_score);
    }
    if (gpio0__get_level(6, 0)) {
      vTaskDelay(200);
    }
    if (gpio0__get_level(7, 0)) {
      vTaskDelay(200);
    }
    if (gpio0__get_level(26, 0)) {
      vTaskDelay(200);
    }
    vTaskDelay(5);
  }
}

void player_logic(void *p) {
  vTaskSuspend(NULL);
  // onboard buttons
  // gpio0__set_as_input(30, 0);
  // gpio0__set_as_input(29, 0);
  // gpio0__set_as_input(15, 1);
  // gpio0__set_as_input(19, 1);

  gpio0__set_as_input(6, 0);  // external sw1
  gpio0__set_as_input(7, 0);  // external sw2
  gpio0__set_as_input(8, 0);  // external sw3
  gpio0__set_as_input(9, 0);  // external sw4
  gpio0__set_as_input(26, 0); // external sw5

  // LPC_IOCON->P1_15 &= ~(3 << 3);
  // LPC_IOCON->P1_15 |= (1 << 3);
  // LPC_IOCON->P1_19 &= ~(3 << 3);
  // LPC_IOCON->P1_19 |= (1 << 3);

  while (1) {
    if (gpio0__get_level(8, 0)) { // sw3
      tetris__rotate(&current_piece);
      vTaskDelay(200);
    }
    if (gpio0__get_level(6, 0)) { // sw1
      tetris__player_move_left(&current_piece);
      vTaskDelay(200);
    }
    if (gpio0__get_level(7, 0)) { // sw2
      tetris__player_move_right(&current_piece);
      vTaskDelay(200);
    }
    if (gpio0__get_level(9, 0)) { // sw4
      if (save_piece_state == 0 || save_piece_state == 1) {
        saved_piece_id = tetris__save_piece(&current_piece);
      } else if (save_piece_state == 2) {
        ;
      }
      save_was_pressed = true;
      vTaskDelay(200);
    }
    if (gpio0__get_level(26, 0)) { //sw5
      game_place_piece(&current_piece);
      vTaskDelay(200);
    }
    vTaskDelay(5);
  }
}

int main(void) {
  led_display_init();
  uint32_t pclk = clock__get_peripheral_clock_hz;
  uart__init(UART__3, pclk, 115200);
  gpio__construct_with_function(GPIO__PORT_4, 28, GPIO__FUNCTION_2); // TX

  // game_draw_display();
  // game_draw_game_board(0);
  // game_board_timer(0);

  xTaskCreate(display, "display", 1024U / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  xTaskCreate(title_color, "title_color", 1048U / sizeof(void *), NULL, PRIORITY_LOW, &title_color_handle);
  xTaskCreate(game_logic, "tetris_logic", 1024U / sizeof(void *), NULL, PRIORITY_MEDIUM, &game_logic_handle);
  xTaskCreate(clear_board_animation, "clear_board", 512U / sizeof(void *), NULL, PRIORITY_HIGH, &game_over_handle);
  xTaskCreate(player_logic, "player", 512U / sizeof(void *), NULL, PRIORITY_MEDIUM, &game_player_logic_handle);
  xTaskCreate(timer, "timer", 512U / sizeof(void *), NULL, PRIORITY_HIGH, &game_timer_handle);
  xTaskCreate(menu_logic, "menu", 512U / sizeof(void *), NULL, PRIORITY_HIGH, &game_menu_handle);
  xTaskCreate(menu_title, "menu_title", 512U / sizeof(void *), NULL, PRIORITY_LOW, &game_menu_title);

  sj2_cli__init();

  puts("Starting RTOS");
  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails

  return 0;
}