#include "tetris_menu.h"

void tetris__menu_draw_name(uint8_t position) {
  uint32_t r31 = 0;
  uint32_t r30 = 0;
  uint32_t r29 = 0;
  uint32_t r28 = 0;
  uint32_t r27 = 0;

  r31 = 0b1111101110111110111001110011110;
  r30 = 0b0010001000001000100100100100000;
  r29 = 0b0010001100001000111000100011100;
  r28 = 0b0010001000001000101000100000010;
  r27 = 0b0010001110001000100101110111100;

  led_board_blue[31] &= ~(7 << position);
  led_board_blue[31] |= r31 & (7 << position);
  led_board_green[31] = 0;
  led_board_green[31] |= r31 & (7 << position);

  led_board_blue[30] &= ~(7 << position);
  led_board_blue[30] |= r30 & (7 << position);
  led_board_green[30] = 0;
  led_board_green[30] |= r30 & (7 << position);

  led_board_blue[29] &= ~(7 << position);
  led_board_blue[29] |= r29 & (7 << position);
  led_board_green[29] = 0;
  led_board_green[29] |= r29 & (7 << position);

  led_board_blue[28] &= ~(7 << position);
  led_board_blue[28] |= r28 & (7 << position);
  led_board_green[28] = 0;
  led_board_green[28] |= r28 & (7 << position);

  led_board_blue[27] &= ~(7 << position);
  led_board_blue[27] |= r27 & (7 << position);
  led_board_green[27] = 0;
  led_board_green[27] |= r27 & (7 << position);
}

void tetris__draw_menu(uint8_t difficulty, uint8_t clr_board) {

  if(clr_board == 1){
    for(int i = 0; i <26; i ++){
      led_board_green[i] = 0;
      led_board_red[i] = 0;
      led_board_blue[i] = 0;
    }
  } else {
    for(int i = 0; i <5; i ++){
      led_board_green[15+i] = 0;
      led_board_red[15+i] = 0;
      led_board_blue[15+i] = 0;
    }
  }
  if (difficulty == 0) {
    led_board_green[19] = 0b00000011110011000111010001000000;
    led_board_green[18] = 0b00000010000100101000001010000000;
    led_board_green[17] = 0b00000011100111100110000100000000;
    led_board_green[16] = 0b00000010000100100001000100000000;
    led_board_green[15] = 0b00000011110100101110000100000000;
  } else if (difficulty == 1) {
    led_board_blue[19] = 0b10001011110111001000100110010000;
    led_board_blue[18] = 0b11001010010100101101101001010000;
    led_board_blue[17] = 0b10101010010111001010101111010000;
    led_board_blue[16] = 0b10011010010101001010101001010000;
    led_board_blue[15] = 0b10001011110100101010101001011110;
  } else if (difficulty == 2) {
    led_board_red[19] = 0b00000010010011001110011100000000;
    led_board_red[18] = 0b00000010010100101001010010000000;
    led_board_red[17] = 0b00000011110111101110010010000000;
    led_board_red[16] = 0b00000010010100101010010010000000;
    led_board_red[15] = 0b00000010010100101001011100000000;
  }
        
    led_board_green[13] = 0xFFFFFFFF;
    led_board_green[21] = 0xFFFFFFFF;

    led_board_blue[10] = 0b00110011001001110001100000000000;
    led_board_blue[9] =  0b01000100010101001010010000000000;
    led_board_blue[8] =  0b01110100010101110011100000000000;
    led_board_blue[7] =  0b00010100010101010010000000000000;
    led_board_blue[6] =  0b01100011001001001001100000000000;

  return;
}


void tetris__menu_score_draw(int number) {

  uint32_t menu_score_board[5] = {0};

  int temp = 0;

  if (number > 999999)
    number = 999999;
  {
    // First digit
    for (int i = 0; i < 8; i++) {
      if (i == 0) {
        temp = number % (10);
      } else if (i == 1) {
        if (number > 9)
          temp = (number % 100) / (10);
        else
          temp = 0;
      } else if (i == 2) {
        if (number > 99)
          temp = (number % 1000) / (100);
        else
          temp = 0;
      } else if (i == 3) {
        if (number > 999)
          temp = (number % 10000) / (1000);
        else
          temp = 0;
      } else if (i == 4) {
        if (number > 9999)
          temp = (number % 100000) / (10000);
        else
          temp = 0;
      } else if (i == 5) {
        if (number > 99999)
          temp = (number % 1000000) / (100000);
        else
          temp = 0;
      } else if (i == 6) {
        if (number > 999999)
          temp = (number % 10000000) / (1000000);
        else
          temp = 0;
      }

      if (temp == 0) {
        menu_score_board[4] |= (0b111 << ((4 * i)));
        menu_score_board[3] |= (0b101 << ((4 * i)));
        menu_score_board[2] |= (0b101 << ((4 * i)));
        menu_score_board[1] |= (0b101 << ((4 * i)));
        menu_score_board[0] |= (0b111 << ((4 * i)));
      } else if (temp == 1) {
        menu_score_board[4] |= (0b001 << ((4 * i)));
        menu_score_board[3] |= (0b001 << ((4 * i)));
        menu_score_board[2] |= (0b001 << ((4 * i)));
        menu_score_board[1] |= (0b001 << ((4 * i)));
        menu_score_board[0] |= (0b001 << ((4 * i)));
      } else if (temp == 2) {
        menu_score_board[4] |= (0b111 << ((4 * i)));
        menu_score_board[3] |= (0b001 << ((4 * i)));
        menu_score_board[2] |= (0b111 << ((4 * i)));
        menu_score_board[1] |= (0b100 << ((4 * i)));
        menu_score_board[0] |= (0b111 << ((4 * i)));
      } else if (temp == 3) {
        menu_score_board[4] |= (0b111 << ((4 * i)));
        menu_score_board[3] |= (0b001 << ((4 * i)));
        menu_score_board[2] |= (0b111 << ((4 * i)));
        menu_score_board[1] |= (0b001 << ((4 * i)));
        menu_score_board[0] |= (0b111 << ((4 * i)));
      } else if (temp == 4) {
        menu_score_board[4] |= (0b101 << ((4 * i)));
        menu_score_board[3] |= (0b101 << ((4 * i)));
        menu_score_board[2] |= (0b111 << ((4 * i)));
        menu_score_board[1] |= (0b001 << ((4 * i)));
        menu_score_board[0] |= (0b001 << ((4 * i)));
      } else if (temp == 5) {
        menu_score_board[4] |= (0b111 << ((4 * i)));
        menu_score_board[3] |= (0b100 << ((4 * i)));
        menu_score_board[2] |= (0b110 << ((4 * i)));
        menu_score_board[1] |= (0b001 << ((4 * i)));
        menu_score_board[0] |= (0b110 << ((4 * i)));
      } else if (temp == 6) {
        menu_score_board[4] |= (0b111 << ((4 * i)));
        menu_score_board[3] |= (0b100 << ((4 * i)));
        menu_score_board[2] |= (0b111 << ((4 * i)));
        menu_score_board[1] |= (0b101 << ((4 * i)));
        menu_score_board[0] |= (0b111 << ((4 * i)));
      } else if (temp == 7) {
        menu_score_board[4] |= (0b111 << ((4 * i)));
        menu_score_board[3] |= (0b001 << ((4 * i)));
        menu_score_board[2] |= (0b001 << ((4 * i)));
        menu_score_board[1] |= (0b001 << ((4 * i)));
        menu_score_board[0] |= (0b001 << ((4 * i)));
      } else if (temp == 8) {
        menu_score_board[4] |= (0b111 << ((4 * i)));
        menu_score_board[3] |= (0b101 << ((4 * i)));
        menu_score_board[2] |= (0b111 << ((4 * i)));
        menu_score_board[1] |= (0b101 << ((4 * i)));
        menu_score_board[0] |= (0b111 << ((4 * i)));
      } else if (temp == 9) {
        menu_score_board[4] |= (0b111 << ((4 * i)));
        menu_score_board[3] |= (0b101 << ((4 * i)));
        menu_score_board[2] |= (0b111 << ((4 * i)));
        menu_score_board[1] |= (0b001 << ((4 * i)));
        menu_score_board[0] |= (0b001 << ((4 * i)));
      }
    }
  }
  led_board_blue[4] = menu_score_board[4];
  led_board_blue[3] = menu_score_board[3];
  led_board_blue[2] = menu_score_board[2];
  led_board_blue[1] = menu_score_board[1];
  led_board_blue[0] = menu_score_board[0];

}
