#include "led_display_driver.h"

void tetris__menu_draw_name(uint8_t position);
void tetris__draw_menu(uint8_t difficulty, uint8_t clr_board);
void tetris__menu_score_draw(int number);
