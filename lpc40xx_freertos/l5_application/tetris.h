#include "led_display_driver.h"

typedef struct {
  uint8_t x1, y1, x2, y2, x3, y3, x4, y4;
  uint8_t px1, py1, px2, py2, px3, py3, px4, py4;
  uint8_t color;
  uint8_t complete;
} piece_in_play;

typedef enum {
  blue_ricky = 1,
  cleaveland_z = 2,
  hero = 3,
  rhode_z = 4,
  teewee = 5,
  smashboy = 6,
  white_ricky = 7,
} piece_color_e;

// GAME FUNCTIONS
piece_in_play init_play_piece(int piece_selector);
uint8_t game_place_piece(piece_in_play *piece);
uint8_t board_check_occupied(uint8_t column, uint8_t row);
void game_draw_piece(piece_in_play *piece, uint8_t r, uint8_t g, uint8_t b, uint8_t prev);

uint8_t board_check_previous_location(piece_in_play *piece, uint8_t x, uint8_t y);
uint8_t board_check_piece(piece_in_play *piece);
uint8_t game_check_full_lines(void);
void tetris__rotate(piece_in_play *piece);
void tetris__player_move_right(piece_in_play *piece);
void tetris__player_move_left(piece_in_play *piece);
uint8_t tetris__save_piece(piece_in_play *piece);

// Fail conditions
void board_clear_game_board(uint8_t index);
uint8_t board_check_end_game(piece_in_play *piece);